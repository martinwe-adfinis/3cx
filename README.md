3cx
===

**3cx** is a wrapper scripts for launching a Web browser that shows the [3CX Web
phone service][1].

The choice of Web browser is set in an INI-style configuration file. The Web
browser is then launched by a specific *backend* script; the available backends
are currently:

* `3cx-backend-chromium`: launches Chromium with `--app`;
* `3cx-backend-firefox`: launches Firefox with `--kiosk`;
* `3cx-backend-qutebrowser`: launches Qutebrowser in a dedicated session.


Build
-----

3cx can be built with `make`. It accepts the `PREFIX` parameter. Example:

    make PREFIX=/usr

### Build-time dependencies

* `coreutils`
* `make`
* `sed`
* `sh`


Install
-------

The built 3cx can be installed with `make install`. It accepts the `INSTALLDIR`
parameter for staged installations. Example:

    make INSTALLDIR=$pkgdir install

The components can also be installed individually (if you wish to split the main
program from the backends into separate packages). Examples:

    make install_3cx                        # only 3cx itself
    make install_backends                   # only the backends
    make BACKENDS=chromium install_backends # only the Chromium backend

### Runtime dependencies for `3cx`

* `coreutils`
* `glibc`
* `sh`

### Runtime dependencies for `3cx-backend-chromium`

* `chromium`
* `sh`

### Runtime dependencies for `3cx-backend-firefox`

* `firefox`
* `sh`

It is recommended to run Firefox [with the MOZDIR wrapper][2].

### Runtime dependencies for `3cx-backend-qutebrowser`

* `qutebrowser`
* `sh`

It is recommended to run Qutebrowser [with the session wrapper][3].


Usage
-----

Run `3cx` with `-h` to get an overview of available command line options.

See the comments in the example configuration file for how to configure 3cx.

When configured as desired, one can usually run it just like that:

    3cx

Documentation in the form of manpages are not available (yet).


[1]: https://3cx.ch/
[2]: https://github.com/ayekat/dotfiles/blob/master/lib/dotfiles/bin/firefox
[3]: https://github.com/ayekat/dotfiles/blob/master/lib/dotfiles/bin/qutebrowser
