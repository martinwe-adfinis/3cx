BACKENDS ?= chromium firefox qutebrowser

BUILDDIR ?= build
BUILDDIR_EXEC = ${BUILDDIR}/bin
BUILDDIR_DOC = ${BUILDDIR}/doc
BUILDDIR_BACKENDS = ${BUILDDIR}/backends
BUILDDIR_PREFIXINFO = ${BUILDDIR}/meta

INSTALLDIR ?=

# INSTALLATION =================================================================

PREFIX ?= /usr/local
PREFIX_EXEC = ${PREFIX}/bin
PREFIX_DOC = ${PREFIX}/share/doc/3cx
PREFIX_BACKENDS = ${PREFIX}/share/3cx/backends

# BUILD ========================================================================

PREFIXINFO_BUILT = ${BUILDDIR_PREFIXINFO}/prefix
EXEC_BUILT = ${BUILDDIR_EXEC}/3cx
DOC_BUILT = ${BUILDDIR_DOC}/3cx.conf.example
BACKENDS_BUILT = $(BACKENDS:%=${BUILDDIR_BACKENDS}/3cx-backend-%)

.PHONY: all
all: ${PREFIXINFO_BUILT} ${EXEC_BUILT} ${DOC_BUILT} ${BACKENDS_BUILT}

${BUILDDIR}:
	mkdir -p '$@'
${BUILDDIR_EXEC}: ${BUILDDIR}
	mkdir -p '$@'
${BUILDDIR_DOC}: ${BUILDDIR}
	mkdir -p '$@'
${BUILDDIR_BACKENDS}: ${BUILDDIR}
	mkdir -p '$@'
${BUILDDIR_PREFIXINFO}: ${BUILDDIR}
	mkdir -p '$@'

${BUILDDIR_EXEC}/%: % | ${BUILDDIR_EXEC}
	sed -e "s|^\\(readonly PREFIX=\\).*\$$|\\1'${PREFIX}'|" '$<' >'$@'
${BUILDDIR_DOC}/%: % | ${BUILDDIR_DOC}
	cp '$<' '$@'
${BUILDDIR_BACKENDS}/%: % | ${BUILDDIR_BACKENDS}
	cp '$<' '$@'
${BUILDDIR_PREFIXINFO}/%: | ${BUILDDIR_PREFIXINFO}
	echo '_PREFIX = ${PREFIX}' >'$@'

.PHONY: clean
clean:
	rm -r build

# STAGE ========================================================================

-include ${PREFIXINFO_BUILT}
INSTALLDIR_EXEC = ${INSTALLDIR}${_PREFIX}/bin
INSTALLDIR_DOC = ${INSTALLDIR}${_PREFIX}/share/doc/3cx
INSTALLDIR_BACKENDS = ${INSTALLDIR}${_PREFIX}/share/3cx/backends

EXEC_INSTALLED = ${INSTALLDIR_EXEC}/3cx
DOC_INSTALLED = ${INSTALLDIR_DOC}/3cx.conf.example
BACKENDS_INSTALLED = $(BACKENDS:%=${INSTALLDIR_BACKENDS}/3cx-backend-%)

.PHONY: install install_3cx install_backends
install: install_3cx install_backends
install_3cx: ${EXEC_INSTALLED} ${DOC_INSTALLED}
install_backends: ${BACKENDS_INSTALLED}

${INSTALLDIR_EXEC}/%: ${BUILDDIR_EXEC}/%
	install -Dm 0755 '$<' '$@'
${INSTALLDIR_DOC}/%: ${BUILDDIR_DOC}/%
	install -Dm 0644 '$<' '$@'
${INSTALLDIR_BACKENDS}/%: ${BUILDDIR_BACKENDS}/%
	install -Dm 0755 '$<' '$@'
